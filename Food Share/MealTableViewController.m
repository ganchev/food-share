//
//  MealTableViewController.m
//  Food Share
//
//  Created by Miroslav Ganchev on 19/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import "MealTableViewController.h"

@interface MealTableViewController ()

@end

@implementation MealTableViewController

@synthesize meals;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    meals = [[NSArray alloc] initWithObjects:@"Peperoni_pizza.jpg", @"260px-MussakasMeMelitsanesKePatates01.JPG", @"img-meal-1@2x.png", @"img-meal-2@2x.png", @"img-meal-3@2x.png",nil];
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    [tempImageView setFrame:self.tableView.frame];
    
    self.tableView.backgroundView = tempImageView;
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(appDelegate.locationManager == nil) {
        appDelegate.locationManager = [[CLLocationManager alloc] init];
        appDelegate.locationManager.delegate = appDelegate;
        appDelegate.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        appDelegate.locationManager.distanceFilter = kCLDistanceFilterNone;
        appDelegate.locationDelegate = self;
    }
    //wheather will be saved in the delegate method of the location manager
    [appDelegate.locationManager startUpdatingLocation];
    self.navigationController.navigationBarHidden = NO;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return meals.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Meal";
    MealCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.mealImage.image = [UIImage imageNamed:[meals objectAtIndex:indexPath.row]];
    [cell setDetailShadow];
    // Configure the cell...
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */


- (void)deselectAllSortButtons {
    for(UIView *view in self.sortView.subviews) {
        //is it button
        if([view respondsToSelector:@selector(setSelected:)] && ![[(UIButton *)view titleForState:UIControlStateNormal] isEqualToString:@"Search"]) {
            [(UIButton *)view setSelected:NO];
            [[(UIButton *)view titleLabel] setFont:[UIFont systemFontOfSize:13.0f]];
            view.backgroundColor = [UIColor clearColor];
        }
    }
}

- (void)setSelectedSortButton:(UIButton *)button {
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 320, 400) animated:NO];
    [self deselectAllSortButtons];
    [button setSelected:YES];
    [button setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.4f]];
    [self onCloseSort:self.btnSort];
}

- (IBAction)onOpenSort:(id)sender {
    if(self.sortView.frame.origin.x == -160)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f];
        self.tableView.frame = CGRectMake(160, self.tableView.frame.origin.y, 320, self.tableView.frame.size.height);
        self.sortView.frame = CGRectMake(-1, self.sortView.frame.origin.y, self.sortView.frame.size.width, self.sortView.frame.size.height);
        //self.btnSort.selected = YES;
        
        [UIView commitAnimations];
    }
    else
    {
        [self onCloseSort:sender];
    }
}

- (IBAction)onCloseSort:(id)sender {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    self.tableView.frame = CGRectMake(0, self.tableView.frame.origin.y, 320, self.tableView.frame.size.height);
    self.sortView.frame = CGRectMake(-160, self.sortView.frame.origin.y, self.sortView.frame.size.width, self.sortView.frame.size.height);
    //self.btnSort.selected = NO;
    [UIView commitAnimations];
}

- (void)closeSortNotAnimated {
    self.tableView.frame = CGRectMake(0, self.tableView.frame.origin.y, 320, self.tableView.frame.size.height);
    self.sortView.frame = CGRectMake(-160, self.sortView.frame.origin.y, self.sortView.frame.size.width, self.sortView.frame.size.height);
    //self.btnSort.selected = NO;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
