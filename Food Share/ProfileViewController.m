//
//  ProfileViewController.m
//  Food Share
//
//  Created by Miroslav Ganchev on 20/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //rating
    
    DYRateView *rateView1 = [self.rateView initWithFrame:self.rateView.frame fullStar:[UIImage imageNamed:@"star_on.png"] emptyStar:[UIImage imageNamed:@"star.png"]];
    rateView1.padding = 5;
    self.rateView.alignment = RateViewAlignmentLeft;
    self.rateView.editable = YES;
    self.rateView.rate = 3.3;
    self.rateView.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)rateView:(DYRateView *)rateView changedToNewRate:(NSNumber *)rate{
    
}
@end
