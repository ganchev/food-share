//
//  MealCell.m
//  Food Share
//
//  Created by Miroslav Ganchev on 19/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import "MealCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation MealCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        //[self commonInit];
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setDetailShadow];
    }
    return self;
}

- (void)setDetailShadow
{
    self.detailView.layer.masksToBounds = NO;
    self.detailView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.detailView.layer.shadowOpacity = .38f;
    self.detailView.layer.shadowRadius = 5;
    self.detailView.layer.shadowOffset = CGSizeMake(1.0f, 0.0f);
    //self.detailView.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
}

- (IBAction)onJoin:(id)sender {
}
@end
