//
//  PeopleTableViewController.h
//  Food Share
//
//  Created by Miroslav Ganchev on 19/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeopleTableViewController : UITableViewController

@end
