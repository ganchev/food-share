//
//  main.m
//  Food Share
//
//  Created by Miroslav Ganchev on 18/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
