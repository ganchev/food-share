//
//  ProfileViewController.h
//  Food Share
//
//  Created by Miroslav Ganchev on 20/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYRateView.h"

@interface ProfileViewController : UIViewController <DYRateViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet DYRateView *rateView;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end
