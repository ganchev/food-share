//
//  MapViewController.m
//  Food Share
//
//  Created by Miroslav Ganchev on 19/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import "MapViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface MapViewController ()

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:appDelegate.currentLocation.coordinate radius:400];
    [self.mapView addOverlay:circle];
    [self.mapView setCenterCoordinate:appDelegate.currentLocation.coordinate];
    [self.mapView setZoomEnabled:YES];
    MKCoordinateRegion region; //create a region.  No this is not a pointer
    region.center = appDelegate.currentLocation.coordinate;  // set the region center to your current location
    MKCoordinateSpan span; // create a range of your view
    span.latitudeDelta = BASE_RADIUS / 3;  // span dimensions.  I have BASE_RADIUS defined as 0.0144927536 which is equivalent to 1 mile
    span.longitudeDelta = BASE_RADIUS / 3;  // span dimensions
    region.span = span; // Set the region's span to the new span.
    [self.mapView setRegion:region animated:YES];
}

- (MKOverlayView *)mapView:(MKMapView *)map viewForOverlay:(id <MKOverlay>)overlay
{
    MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
    circleView.strokeColor = [UIColor blueColor];
    circleView.fillColor = [[UIColor blueColor] colorWithAlphaComponent:0.4];
    return circleView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
