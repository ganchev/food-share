//
//  MealTableViewController.h
//  Food Share
//
//  Created by Miroslav Ganchev on 19/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MealCell.h"
#import <CoreLocation/CoreLocation.h>

@interface MealTableViewController : UITableViewController
@property (strong) NSArray *meals;
@property (weak, nonatomic) IBOutlet UIView *sortView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSort;

- (IBAction)onOpenSort:(id)sender;

@end
