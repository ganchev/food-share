//
//  MapViewController.h
//  Food Share
//
//  Created by Miroslav Ganchev on 19/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

#define BASE_RADIUS 0.0544927536


@interface MapViewController : UIViewController <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
