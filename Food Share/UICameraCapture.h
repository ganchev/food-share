//
//  UICameraCapture.h
//  Stylitics
//
//  Created by Miroslav Ganchev on 2/25/13.
//  Copyright (c) 2013 Stylitics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

@protocol UICameraCapture <NSObject>
-(void) FinishPickingImage:(UIImage *)image;

@end

@interface UICameraCapture : UIViewController
<UIImagePickerControllerDelegate,
UINavigationControllerDelegate>

@property BOOL newMedia;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) id delegate;
- (IBAction)useCamera:(id)sender;
- (IBAction)useCameraRoll:(id)sender;
@end