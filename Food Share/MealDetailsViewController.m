//
//  MealDetailsViewController.m
//  Food Share
//
//  Created by Miroslav Ganchev on 19/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import "MealDetailsViewController.h"

@interface MealDetailsViewController ()

@end

@implementation MealDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.afImgViewer.images = [self images];
    self.afImgViewer.contentMode = UIViewContentModeScaleAspectFill;
    self.afImgViewer.pageControl.backgroundColor = [UIColor clearColor];
    self.afImgViewer.pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    self.afImgViewer.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    
    self.postImage = [self.afImgViewer.images objectAtIndex:0];
    self.postText = self.shortDiscribtions.text;
    
//    self.profileImage.layer.cornerRadius = 20.0f;
//    self.profileImage.clipsToBounds = YES;
    [self setRoundedView:self.profileImage toDiameter:40.0];
    self.profileImage.clipsToBounds = YES;
    
    [self.detailText sizeToFit];
    NSInteger wd = self.detailText.frame.origin.y;
    NSInteger ht = self.detailText.frame.size.height;
    
    float sizeOfContent = wd+ht;
    
    self.mealScrollview.contentSize = CGSizeMake(self.mealScrollview.frame.size.width, sizeOfContent);
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}

-(NSArray *) images
{
    NSArray *imageNames = [NSArray arrayWithObjects:@"Peperoni_pizza.jpg", @"Peperoni.jpeg", @"Pepperoni_pizza.jpg", @"pizza de peperoni.jpg", nil];
    NSMutableArray *images = [NSMutableArray array];
    
    for (NSString *imageName in imageNames) [images addObject:[UIImage imageNamed:imageName]];
    
    return images;
}

-(void)setRoundedView:(UIImageView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBook:(id)sender {
}

- (IBAction)onShare:(id)sender{
    NSArray *activityItems = nil;
    
    if (self.postImage != nil) {
        activityItems = @[self.postText, self.postImage];
    } else {
        activityItems = @[self.postText];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

- (IBAction)OnMap:(id)sender {
    self.mapContainer.hidden = NO;
    self.peopleContainer.hidden = YES;
    self.detailText.hidden = YES;
    NSInteger wd = self.mapContainer.frame.origin.y;
    NSInteger ht = self.mapContainer.frame.size.height;
    float sizeOfContent = wd+ht+10;
    self.mealScrollview.contentSize = CGSizeMake(self.mealScrollview.frame.size.width, sizeOfContent);
    [self.underLine setFrame:CGRectMake(110, self.underLine.frame.origin.y, self.underLine.frame.size.width, self.underLine.frame.size.height)];
}

- (IBAction)onPeople:(id)sender {
    self.mapContainer.hidden = YES;
    self.peopleContainer.hidden = NO;
    self.detailText.hidden = YES;
    NSInteger wd = self.peopleContainer.frame.origin.y;
    NSInteger ht = self.peopleContainer.frame.size.height;
    
    [self.underLine setFrame:CGRectMake(210, self.underLine.frame.origin.y, self.underLine.frame.size.width, self.underLine.frame.size.height)];
    
    float sizeOfContent = wd+ht+10;
    self.mealScrollview.contentSize = CGSizeMake(self.mealScrollview.frame.size.width, sizeOfContent);
}

- (IBAction)onDetails:(id)sender {
    self.mapContainer.hidden = YES;
    self.peopleContainer.hidden = YES;
    self.detailText.hidden = NO;
    NSInteger wd = self.detailText.frame.origin.y;
    NSInteger ht = self.detailText.frame.size.height;
    
    [self.underLine setFrame:CGRectMake(10, self.underLine.frame.origin.y, self.underLine.frame.size.width, self.underLine.frame.size.height)];
    
    float sizeOfContent = wd+ht+10;
    self.mealScrollview.contentSize = CGSizeMake(self.mealScrollview.frame.size.width, sizeOfContent);
}
- (BOOL)prefersStatusBarHidden {
    return NO;
}
@end
