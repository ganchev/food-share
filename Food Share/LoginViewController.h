//
//  LoginViewController.h
//  Food Share
//
//  Created by Miroslav Ganchev on 20/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
- (IBAction)onBack:(id)sender;

@end
