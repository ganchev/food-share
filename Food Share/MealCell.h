//
//  MealCell.h
//  Food Share
//
//  Created by Miroslav Ganchev on 19/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MealCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mealImage;
@property (weak, nonatomic) IBOutlet UILabel *mealTitle;
@property (weak, nonatomic) IBOutlet UILabel *mealPlace;
@property (weak, nonatomic) IBOutlet UILabel *mealShortDesc;
@property (weak, nonatomic) IBOutlet UILabel *mealTime;
@property (weak, nonatomic) IBOutlet UIView *detailView;
- (IBAction)onJoin:(id)sender;
- (void)setDetailShadow;

@end
