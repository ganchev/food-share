//
//  MealDetailsViewController.h
//  Food Share
//
//  Created by Miroslav Ganchev on 19/10/2013.
//  Copyright (c) 2013 Miroslav Ganchev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFImageViewer.h"
#import <QuartzCore/QuartzCore.h>

@interface MealDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong) NSString *postText;
@property (strong) UIImage *postImage;
@property (strong) NSArray *mealImages;
@property (weak, nonatomic) IBOutlet AFImageViewer *afImgViewer;
@property (weak, nonatomic) IBOutlet UILabel *detailText;
@property (weak, nonatomic) IBOutlet UIScrollView *mealScrollview;
@property (weak, nonatomic) IBOutlet UIView *mapContainer;
@property (weak, nonatomic) IBOutlet UIView *peopleContainer;
@property (weak, nonatomic) IBOutlet UIImageView *underLine;
@property (weak, nonatomic) IBOutlet UILabel *shortDiscribtions;


- (IBAction)OnMap:(id)sender;
- (IBAction)onBook:(id)sender;
- (IBAction)onShare:(id)sender;
- (IBAction)onPeople:(id)sender;
- (IBAction)onDetails:(id)sender;

@end
